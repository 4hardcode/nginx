# Загрузка базового образа
FROM ubuntu:24.04

# Установка дополнительных пакетов для выполнения последующих команд
RUN apt-get update && apt-get install -y curl gnupg2 ca-certificates lsb-release

# Импорт ключа nginx, чтобы apt-get мог проверить подлинность пакетов.
RUN curl https://nginx.org/keys/nginx_signing.key | gpg --dearmor | tee /usr/share/keyrings/nginx-archiv>

# Добавление репозитория стабильной версии Nginx
RUN echo "deb [signed-by=/usr/share/keyrings/nginx-archive-keyring.gpg]  http://nginx.org/packages/mainl>

# Синхронизация индексов файлов, доступных из репозитория и установка пакета Nginx
RUN apt-get update && apt-get install -y nginx

# Копирование файла index.html с хостовой машины в docker контейнер в директорию /usr/share/nginx/html
COPY ./index.html /usr/share/nginx/html

# Открытие порта Nginx по умолчанию
EXPOSE 80

# Запуск Nginx при запуске контейнера
CMD ["nginx", "-g", "daemon off;"]
